console.log(__dirname);
const express = require("express");
const server = express();
const usersRouter = require('./routes/users');

server.set("view engine", "ejs");
server.use(express.static(__dirname + "/public"))
server.use(express.urlencoded({extended: true}));

server.get("/", (req, res) =>{
    res.render("index");
});

server.use("/users", usersRouter);

server.listen(8000);
